#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Feb 23 13:55:05 2023

@author: putta
"""

import numpy as np



def gauss_seidel(A, b, x0, tol=1e-10, max_iter=1000):
    """
    Solves a system of linear equations Ax=b using the Gauss-Seidel iteration method.

    Parameters:
    A (array): the matrix of coefficients
    b (array): the right-hand side vector
    x0 (array): the initial guess for the solution vector
    tol (float): the tolerance for the error (default: 1e-10)
    max_iter (int): the maximum number of iterations (default: 1000)

    Returns:
    x (array): the solution vector
    iter (int): the number of iterations performed
    """
    n = len(b)
    x = np.copy(x0)
    iter = 0
    while iter < max_iter:
        x_new = np.zeros(n)
        for i in range(n):
            s1 = np.dot(A[i, :i], x_new[:i])
            s2 = np.dot(A[i, i + 1:], x[i + 1:])
            x_new[i] = (b[i] - s1 - s2) / A[i, i]
        if np.linalg.norm(x_new - x) < tol:
            return x_new, iter
        x = np.copy(x_new)
        iter += 1
    return x, iter


"""
Lets solve
3x + y + z = 1
x + 4y + z = 4
x + y + 5z = 5
"""

A = np.array([[3, 1, 1], [1, 4, 1], [1, 1, 5]])
b = np.array([1, 4, 5])

x0 = np.zeros(len(b))
x, iter = gauss_seidel(A, b, x0)
print("Solution:", x)
print("Iterations:", iter)
