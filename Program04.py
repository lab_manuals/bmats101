#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Feb 23 12:46:44 2023

@author: putta
"""

from sympy import symbols, diff, solve
import numpy as np

# Define the function
x, y = symbols('x y')
f = x**2 + y**2 - 4*x - 2*y + 4

# Find the critical points
grad_f = [diff(f, var) for var in (x, y)]
solutions = solve(grad_f, (x, y))

# Classify the critical points
for i in range(len(solutions)):
    x = solutions[i][0]
    y = solutions[i][1]
    H = np.array([[diff(diff(f, x), x), diff(diff(f, x), y)], 
                  [diff(diff(f, y), x), diff(diff(f, y), y)]])
    eigenvalues = np.linalg.eig(H)[0]
    if eigenvalues[0] > 0 and eigenvalues[1] > 0:
        print(f"The critical point ({x}, {y}) is a local minimum")
    elif eigenvalues[0] < 0 and eigenvalues[1] < 0:
        print(f"The critical point ({x}, {y}) is a local maximum")
    else:
        print(f"The critical point ({x}, {y}) is a saddle point")
