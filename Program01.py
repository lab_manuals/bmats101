#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Feb 23 11:23:22 2023

@author: putta
"""


import matplotlib.pyplot as plt
import numpy as np

option = int(input("1.Cartesian Curve\n2.Polar Curve\nChoose the option : "))

if option == 1:
    # Define the x values
    x = np.linspace(-5, 5, 100)
    
    # Define the function to plot (y = sin(2x) + cos(3x))
    y = np.sin(2*x) * np.cos(3*x)
    #another example
    # y = x**3 + x**2
    # Create the plot
    plt.plot(x, y)
    
    # Add grid
    plt.grid(True)
    
    # Add labels and title
    plt.xlabel('X Axis')
    plt.ylabel('Y Axis')
    plt.title('Cartesian Curve')
    
    # Show the plot
    plt.show()
    
elif option == 2:
    # Define the theta values for the rose curve
    theta = np.linspace(0, 2*np.pi, 1000)
    
    # Define the parameters for the rose curve
    a = 1
    n = 6
    
    # Calculate the radius values for the rose curve
    r = a*np.cos(n*theta)
    
    # Create a polar plot of the rose curve
    plt.polar(theta, r)
    plt.title("Polar Curve")
    # Show the plot
    plt.show()
        