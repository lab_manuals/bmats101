import sympy as sp

# Define the variables
x, y, z = sp.symbols('x y z')

# Define the function f(x, y, z)
f = x**2*y*z + y**2*z*x + z**2*x*y

# Find the partial derivatives of f with respect to x, y, and z
fx = sp.diff(f, x)
fy = sp.diff(f, y)
fz = sp.diff(f, z)

# Print the partial derivatives
print("Partial derivative of f with respect to x:", fx)
print("Partial derivative of f with respect to y:", fy)
print("Partial derivative of f with respect to z:", fz)
print()

# Find the Jacobian of f
J = sp.Matrix([fx, fy, fz])
vars = [x, y, z]
Jacobian = J.jacobian(vars)

# Print the Jacobian
for i in range(len(Jacobian)):
    print("{:25s}".format(str(Jacobian[i])),end = '\t')
    if (i+1)%3 == 0:
        print()
    
