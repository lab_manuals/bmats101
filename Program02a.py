#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Feb 23 15:57:51 2023

@author: putta
"""

import numpy as np
import matplotlib.pyplot as plt

# Define the first polar curve
def r1(theta):
    return 2 * np.cos(theta)

# Define the second polar curve
def r2(theta):
    return np.sin(2 * theta)

# Generate arrays of theta and r values for each curve
theta_range = np.linspace(0, 2 * np.pi, 1000)
r1_values = r1(theta_range)
r2_values = r2(theta_range)

# Calculate the angle between the curves
angle = np.arccos(np.abs(np.trapz(r1_values * r2_values, theta_range)) / (np.trapz(r1_values ** 2, theta_range) * np.trapz(r2_values ** 2, theta_range)))

# Print the angle in degrees
print("The angle between the polar curves is:", np.rad2deg(angle), "degrees")

# Plot the curves
plt.polar(theta_range, r1_values, label='r1')
plt.polar(theta_range, r2_values, label='r2')
plt.legend()
plt.show()
