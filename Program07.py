#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Feb 23 13:38:30 2023

@author: putta
"""

#Solving linear congruences 𝒂𝒙 ≡ 𝒃(𝒎𝒐𝒅 𝒎)

def linear_congruence(a, b, m):
    # check if gcd(a, m) divides b
    gcd, x, y = extended_gcd(a, m)
    if b % gcd != 0:
        return None  # no solution
    else:
        # reduce the congruence
        a //= gcd
        b //= gcd
        m //= gcd
        # find the solution
        x = (x * b) % m
        return x % m

def extended_gcd(a, b):
    if b == 0:
        return a, 1, 0
    else:
        gcd, x, y = extended_gcd(b, a % b)
        return gcd, y, x - (a // b) * y


def main():
    a =  int(input("Enter value for a : "))    
    b =  int(input("Enter value for b : "))    
    m =  int(input("Enter value for m : "))  
    
    if linear_congruence(a, b, m) == None:
        print("There is no solution")
    else:
        print("linear_congruence for", (a, b, m), "is", linear_congruence(a, b, m))
        
main()