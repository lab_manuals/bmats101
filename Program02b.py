#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Feb 23 16:08:35 2023

@author: putta
"""

import numpy as np
import matplotlib.pyplot as plt

# Define the curve
x = np.linspace(-2.5, 2.5, 1000)
y = np.sin(x)

# Calculate the first and second derivatives
dydx = np.gradient(y, x)
d2ydx2 = np.gradient(dydx, x)

# Calculate the curvature
curvature = np.abs(d2ydx2 / (1 + dydx ** 2) ** 1.5)

# Calculate the radius of curvature
radius = 1 / curvature

#Plot the curve
fig, ax1 = plt.subplots()
ax1.plot(x, y, 'b-', label='curve')
ax1.set_xlabel('x')
ax1.set_ylabel('y')
ax1.tick_params(axis='y')

# plt.plot(x, y)
# plt.grid(True)

# Plot the curvature
ax2 = ax1.twinx()
ax2.plot(x, curvature, 'r-', label='curvature')
ax2.set_ylabel('curvature')
ax2.tick_params(axis='y')

# Plot the radius of curvature
ax3 = ax1.twinx()
ax3.spines["right"].set_position(("axes", 1.2))
ax3.plot(x, radius, 'g-', label='radius')
ax3.set_ylabel('radius of curvature')
ax3.tick_params(axis='y')

# Combine the legends
lines = ax1.get_lines() + ax2.get_lines() + ax3.get_lines()
ax1.legend(lines, [line.get_label() for line in lines])

plt.grid(True)
plt.show()
