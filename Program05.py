#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Feb 23 15:04:21 2023

@author: putta
"""

import numpy as np
import matplotlib.pyplot as plt

# Define the differential equation
def f(t, y):
    return t * y - y**2

# Define the time interval and initial condition
t0 = 0
tf = 5
y0 = 1

# Define the step size
h = 0.01

# Initialize the solution array
t = np.arange(t0, tf+h, h)
y = np.zeros(len(t))
y[0] = y0

# Solve the differential equation using Euler's method
for i in range(len(t)-1):
    y[i+1] = y[i] + h * f(t[i], y[i])

# Plot the solution curve
plt.plot(t, y)
plt.xlabel('t')
plt.ylabel('y')
plt.title('Solution Curve for dy/dt = ty - y^2')
plt.show()
